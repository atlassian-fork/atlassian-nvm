class nvm  (
  $version              = $nvm::params::version,
  $default_node_version = $nvm::params::default_node_version,
  $node_versions        = $nvm::params::node_versions,
  $install_dir          = $nvm::params::install_dir,
) inherits nvm::params {
  validate_string($version)
  validate_string($default_node_version)
  validate_array($node_versions)
  if ! member($node_versions, $default_node_version) {
    fail('The main default version isn\'t amongst the listed node versions')
  }
  validate_absolute_path($install_dir)

  class{'nvm::install': } ->
  Class['nvm']
}
